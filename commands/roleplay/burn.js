module.exports = {
    name: "burn",
    usage: "< id / mention >",
    run: async function (client, message, args) {

        if(message.deletable) message.delete();

        let burnUser = message.mentions.members.first() || message.guild.members.cache.get(args[0]);
        if (!burnUser) return message.reply("user not found.");
        if (message.mentions.members.first().user === message.author) return message.reply("you can't roleplay with yourself!");

        let array = ["roasted", "has roasted", "scorched", "burned"];
        await message.channel.send(`${message.author} ${array[Math.round(Math.random() * (array.length - 1))]} ${burnUser}!`);
    }

};
