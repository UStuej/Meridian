module.exports = {
    name: "cuddle",
    usage: "< id / mention >",
    run: async function (client, message, args) {

        if(message.deletable) message.delete();

        let cudUser = message.mentions.members.first() || message.guild.members.cache.get(args[0]);
        if (!cudUser) return message.reply("user not found.");
        if (message.mentions.members.first().user === message.author) return message.reply("you can't roleplay with yourself!");

        let array = ["playfully cuddles with", "happily cuddles with", "gets cold and cuddles with", "cuddles with", "smooches and then cuddles with"];

        await message.channel.send(`${message.author} ${array[Math.round(Math.random() * (array.length - 1))]} ${cudUser}!`);
    }

};